VERDICTS = {
    "ok": "OK",
    "wrong-answer": "WA",
    "time-limit-exceeded": "TL",
    "runtime-error": "RE",
    "memory-limit-exceeded": "ML",
    "presentation-error": "PE",
    "output-limit-exceeded": "OL",
    "idleness-limit-exceeded": "IL",
    "check-failed": "FAIL"
}

# Basic class for test
class Test:
    id = None
    verdict = None
    time = 0
    memory = 0
    points = 0
    
    def __init__(self, config):
        self.id      = config.get("sequenceNumber", None)
        self.verdict = VERDICTS.get(config.get("verdict", None), "UD")
        self.time    = config.get("runningTime", 0)
        self.memory  = config.get("memoryUsed", 0)
        
        pointNode  = config.get("score", {})
        for key in pointNode:
            if key != "scoreType":
                self.points = pointNode[key]

    def isPassed(self):
        return self.verdict == "OK"
    
    def formattedTime(self):
        t = int(self.time)
        if t >= 1000:
            return "{0:>.2f} s".format(t / 1000.0)
        return "{0} ms".format(t)
  
    def formattedMemory(self):
        m = int(self.memory)
        if m > 2**23:
            return "{0} MB".format(m // 2**20)
        if m >= 2**20:
            return "{0:.1f} MB".format(m / 2**20)
        if m > 2**13:
            return "{0} KB".format(m // 2**10)
        if m >= 2**10:
            return "{0:.1f} KB".format(m / 2**10)
        return "{0} bytes".format(m)
