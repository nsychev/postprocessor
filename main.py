#!/usr/bin/python3.4
#
# Postprocessor script for judging contest in Yandex.Contest
#
# version: 4.0.1
# author:  Nikita Sychev (https://github.com/nsychev)
# release: November 5th, 2017

import json, sys

from groups import parseTests
from tests import Test
import traceback
from feedback import feedbackModes, format_points

# function to write data in log
def log(*args):
    print(*args, file=sys.stderr)

# processes Yandex.Contest log
def process_log():
    data = {}
    report = json.loads(input())
    for test_object in report["tests"]:
        test = Test(test_object)
        data[test.id] = test
    return data

# processes config
def process_config(tests):
    group_id = 0
    final_score = 0
    passed_groups = []
    failed_required = -1
    
    config = json.loads(open("config.json").read())

    for groupConfig in config:
        # Settings format for each group
        #
        #     name:          string - custom name of this group                                             default: `group {}`
        #     tests:         string - group tests list, see `groups.py` for format specification            default: []
        #     test_score:    int    - score for each test with OK                                           default: 0
        #     check_partial: bool   - if set, ignores test_score and uses checker points for scoring        default: False
        #     full_score:    int    - if set and group is passed, participant will get reward               default: 0
        #     required:      bool   - if set and group is not passed, valuer won't score next groups        default: False
        #     check_after:   int[]  - if any group from list is not passed, valuer won't score this group   default: []
        #     feedback:      string - type of feedback, see `feedback.py` for possible values               default: `points`
        #
        # Note: groups are indexed from zero (because often "group 0" = "samples")
        
        group = {
            "name": groupConfig.get("name", "group {}".format(group_id)),
            "tests": parseTests(groupConfig.get("tests", "")),
            "test_score": groupConfig.get("test_score", 0),
            "use_partial": groupConfig.get("use_partial", False),
            "full_score": groupConfig.get("full_score", 0),
            "required": groupConfig.get("required", False),
            "check_after": groupConfig.get("check_after", []),
            "feedback": groupConfig.get("feedback", "points")
        }
        
        if failed_required >= 0:
            log("{}: not checked [required group {} failed]".format(group["name"], failed_required))
            continue
        
        for other_group in group["check_after"]:
            if not(other_group in passed_groups):
                log("{}: not checked [required group {} failed]".format(group["name"], other_group))
                continue
        
        group_tests = []
        group_passed = True
        group_score = 0
        
        for test_id in group["tests"]:
            test = tests.get(test_id, Test({"sequenceId": test_id}))
            group_tests.append(test)
            
            if test.verdict != "OK":
                group_passed = False
            elif group["use_partial"]:
                group_score += test.points
            else:
                group_score += group["test_score"]
            
        if group_passed:
            group_score += group["full_score"]
            passed_groups.append(group_id)
        elif group["required"]:
            failed_required = group_id
        
        final_score += group_score
        log(feedbackModes[group["feedback"]](group["name"], group_passed, group_score, group_tests), "\n")
        
        group_id += 1
    
    return final_score

try:
    tests = process_log()
except Exception as e:
    print(-239)
    log("Error occured while parsing logs")
    traceback.print_exc(file=sys.stderr)
    sys.exit(0)

try:
    final_score = process_config(tests)
except Exception as e:
    print(-239)
    log("Error occured while parsing config")
    traceback.print_exc(file=sys.stderr)
    sys.exit(0)

print(final_score)
log("total:", format_points(final_score, short=False))
