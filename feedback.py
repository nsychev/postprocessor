# Convert points to human-readable format
# 
# 1 => 1 point / 1 pt
# 5 => 5 points / 5 pts
def format_points(points, short = True):
    if type(points) is float:
        spec = "{:.2f} {}{}"
    else:
        spec = "{} {}{}"
    return spec.format(
        points,
        "pt" if short else "point",
        "s" if points != 1 else ""
    )
    
# Warning! Options are used to change appearance of some groups
# in participant's report. Even "hidden" and "state" groups will
# be checked. Participant will be able to view final score. It
# will include points earned for "hidden" and "state" groups, so
# participant might probably be able to find out hidden points.

# hidden - shows that this group is hidden, no any information
def hidden(name, passed, points, tests):
    return "{}: hidden".format(name)

# state - shows only whether group passed or failed
def state(name, passed, points, tests):
    return "{}: {}".format(name, "passed" if passed else "failed")

# points - shows only points for whole subgroup
def groupPoints(name, passed, points, tests):
    return "{}: {}".format(name, format_points(points, short=False))
    
# verdicts - `points` + verdict for each test
def verdicts(name, passed, points, tests):
    return groupPoints(name, passed, points, tests) + "\n" + " ".join([test.verdict for test in tests])

# test_points - `verdicts` + points for each passed test
def testPoints(name, passed, points, tests):
    return groupPoints(name, passed, points, tests) + "\n" + " ".join([
        format_points(test.points) if test.verdict == "OK" else test.verdict
        for test in tests
    ])

# full - `verdicts` + time/memory for each test
def full(name, passed, points, tests):
    return groupPoints(name, passed, points, tests) + "\n" + "\n".join([
        "{}: {}, {} / {}".format(test.id, test.verdict, test.formattedTime(), test.formattedMemory())
        for test in tests
    ])

# full_points - `test_points` + time/memory for each test
def fullPoints(name, passed, points, tests):
    return groupPoints(name, passed, points, tests) + "\n" + "\n".join([
        "{}: {}, {} / {}".format(test.id, format_points(test.points) if test.verdict == "OK" else test.verdict, test.formattedTime(), test.formattedMemory())
        for test in tests
    ])

# Possible values for `feedback` option in group configuration
feedbackModes = {
    "hidden": hidden,
    "state": state,
    "points": groupPoints,
    "verdicts": verdicts,
    "test_points": testPoints,
    "full": full,
    "full_points": fullPoints
}
