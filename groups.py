# Exception for parsing string test specifications
class BadTestStringError(Exception):
    def __init__(self, source, annotation = ''):
        self.msg = "Cannot parse string: `{0}`".format(source)
        if len(annotation) > 0:
            self.msg += "\n\n{0}".format(annotation)
    def __str__(self):
        return self.msg

# Function for parsing list of tests
# 
# Rules for valid test list:
#   <test_list> := <test_group>[,<test_list>]
#   <test_group> := <test> | <test>-<test>
def parseTests(tests):
    numbers = []
    
    intervals = [item.strip() for item in tests.split(",")]
    for interval in intervals:
        if len(interval) == 0:
            raise BadTestStringError(tests, "Empty interval")
        
        try:
            bounds = list(map(int, interval.split("-")))
        except Exception:
            raise BadTestStringError(tests, "Bad interval: `{}`".format(interval))
        
        if len(bounds) > 2:
            raise BadTestStringError(tests, "Interval `{}` contains more than two dash-separated integers".format(interval))
        
        left = bounds[0]
        right = bounds[-1]
        for test in range(left, right + 1):
            if test in numbers:
                raise BadTestStringError(tests, "Test `{}` is used more than once".format(test))
            numbers.append(test)
    
    return numbers
